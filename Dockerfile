FROM nginx:latest
WORKDIR /usr/share/nginx/html

COPY build/web .
RUN rm /etc/nginx/conf.d/default.conf || echo "no configuration found"
COPY .docker/nginx/default.conf /etc/nginx/conf.d/nginx.conf

ENV VIRTUAL_HOST=$VIRTUAL_HOST
EXPOSE $EXPOSE_PORT

COPY .docker/entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]